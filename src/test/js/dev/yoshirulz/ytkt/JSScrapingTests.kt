package dev.yoshirulz.ytkt

import dev.yoshirulz.ytkt.StructuredTestData.ChannelIDs
import dev.yoshirulz.ytkt.StructuredTestData.PlaylistData
import dev.yoshirulz.ytkt.StructuredTestData.PlaylistIDs
import dev.yoshirulz.ytkt.StructuredTestData.SearchResults
import dev.yoshirulz.ytkt.StructuredTestData.Usernames
import dev.yoshirulz.ytkt.StructuredTestData.VideoIDs
import dev.yoshirulz.ytkt.datatypes.*
import io.ktor.client.features.*
import kotlin.test.Test

// All tests run in the browser are **live**.

@Test
fun testCCMetadataFromID() {
//	VideoIDs.valid_existent_playable.forEach { // CBB
	VideoIDs.valid_existent_playable_withCCs.forEach {
		assertSucceeds { VideoID.parse(it)!!.getCCMetadata() }
	}
	VideoIDs.valid_nonexistent.forEach {
		assertFailsWith(VideoUnavailableException::class) { VideoID.parse(it)!!.getCCMetadata() }
	}
}

@Test
fun testCCs() {
	VideoIDs.valid_existent_playable_withCCs.forEach {
		assertSucceeds {
			VideoID.parse(it)!!.getCCMetadata().then { it.random().getCaptions() }
		}
	}
}

@Test
fun testChannelFromID() {
	ChannelIDs.valid.map { ChannelID.parse(it)!! }.forEach {
		assertSucceeds { it.getData() }
	}
}

@Test
fun testChannelIDFromUsername() {
	Usernames.valid.map { Username.parse(it)!! }.forEach {
		assertSucceeds { it.getChannelID() }
	}
}

@Test
fun testPlaylistFromID() {
	PlaylistData.rawResults.forEach { (rawID, data) ->
		assertTrueAsync { PlaylistID.parse(rawID)!!.getData().videos.size == data.first }
	}
	PlaylistIDs.valid_unusable.map { PlaylistID.parse(it)!! }.forEach {
		assertFailsWithAsync(ClientRequestException::class) { it.getData() }
	}
}

@Test
fun testSearchResults() {
	SearchResults.queries.forEach {
		assertTrueAsync { it.getPartialResults(30).size <= 30 }
	}
}

@Test
fun testStreaming() {
//	withScraperCache {
//		VideoIDs.valid_nonexistent.map { VideoID.parse(it)!! }.forEach {
//			assertFailsWith(VideoUnavailableException::class) { it.getStreamMetadataSet() }
//		}
//		VideoIDs.valid_existent_unplayable.map { VideoID.parse(it)!! }.forEach {
//			assertFailsWith(VideoUnplayableException::class) { it.getStreamMetadataSet() } // the only datum is a premium video, so we should catch a VideoRequiresPurchaseException
//		}
//		assertSucceeds {
//			VideoIDs.valid_existent_playable.map { VideoID.parse(it)!! }.forEach { videoID ->
//				videoID.getStreamMetadataSet().audioVisual.minByOrNull { it.size }!!
//			}
//		}
//	}
}

@Test
fun testVideoFromID() {
	VideoIDs.valid_existent.map { VideoID.parse(it)!! }.forEach {
		assertSucceedsAsync { it.getData() }
	}
	VideoIDs.valid_nonexistent.map { VideoID.parse(it)!! }.forEach {
		assertFailsWithAsync(VideoUnavailableException::class) { it.getData() }
	}
}
