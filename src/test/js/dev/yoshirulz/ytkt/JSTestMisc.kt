package dev.yoshirulz.ytkt

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asPromise
import kotlinx.coroutines.async
import kotlin.js.Promise
import kotlin.reflect.KClass
import kotlin.test.fail

fun <T: Throwable> assertFailsWith(exceptionClass: KClass<T>, block: () -> Promise<Any?>) {
	block().then({ fail("no throwable where one was expected") }, { if (!exceptionClass.isInstance(it)) fail("caught throwable of wrong type", it) })
}

fun <T: Throwable> assertFailsWithAsync(exceptionClass: KClass<T>, block: suspend CoroutineScope.() -> Any?) {
	assertFailsWith(exceptionClass) { GlobalScope.async(block = block).asPromise() }
}

fun assertSucceeds(block: () -> Promise<Any?>) {
	block().then({}, { fail(it.message) })
}

fun assertSucceedsAsync(block: suspend CoroutineScope.() -> Any?) {
	assertSucceeds { GlobalScope.async(block = block).asPromise() }
}

fun assertTrue(block: () -> Promise<Boolean>) {
	block().then({ if (!it) fail("got false") }, { fail(it.message) })
}

fun assertTrueAsync(block: suspend CoroutineScope.() -> Boolean) {
	assertTrue { GlobalScope.async(block = block).asPromise() }
}
