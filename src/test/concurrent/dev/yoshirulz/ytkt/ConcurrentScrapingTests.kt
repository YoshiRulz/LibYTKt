package dev.yoshirulz.ytkt

import dev.yoshirulz.ytkt.StructuredTestData.ChannelIDs
import dev.yoshirulz.ytkt.StructuredTestData.MockProvider
import dev.yoshirulz.ytkt.StructuredTestData.PlaylistData
import dev.yoshirulz.ytkt.StructuredTestData.PlaylistIDs
import dev.yoshirulz.ytkt.StructuredTestData.SearchResults
import dev.yoshirulz.ytkt.StructuredTestData.Usernames
import dev.yoshirulz.ytkt.StructuredTestData.VideoIDs
import dev.yoshirulz.ytkt.StructuredTestData.VideoInfoMaps
import dev.yoshirulz.ytkt.datatypes.ChannelID
import dev.yoshirulz.ytkt.datatypes.PlaylistID
import dev.yoshirulz.ytkt.datatypes.Username
import dev.yoshirulz.ytkt.datatypes.VideoID
import io.ktor.client.*
import io.ktor.client.engine.mock.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.http.*
import kotlinx.coroutines.delay
import kotlin.test.Ignore
import kotlin.test.Test

private const val LIVE_TEST = false

private val MOCK_HEADER_FORM_ENC = headersOf("Content-Type" to listOf(ContentType.Application.FormUrlEncoded.toString()))

private val MOCK_HEADER_JSON = headersOf("Content-Type" to listOf(ContentType.Application.Json.toString()))

private fun genTestHTTPClient() = HttpClient(MockEngine) {
	engine {
		addHandler { request ->
			val url = request.url
			if (!url.host.endsWith("youtube.com")) error("no mock data for domain ${url.host}")
			try {
				inline fun genResponse(mockProvider: MockProvider, headers: Headers) = mockProvider.mockFromParams(url.parameters).let { (body, status) ->
					if (body == null) respondError(status) else respond(body, status, headers = headers)
				}
				when (url.encodedPath) {
					"/get_video_info" -> genResponse(VideoInfoMaps, MOCK_HEADER_FORM_ENC)
					"/list_ajax" -> genResponse(PlaylistData, MOCK_HEADER_JSON)
					"/search_ajax" -> genResponse(SearchResults, MOCK_HEADER_JSON)
					else -> null
				}?.also { delay(1000L) } ?: error("no mock data for $url")
			} catch (e: Exception) {
				error("generating response for $url threw an exception of type ${e::class.simpleName}: ${e.message ?: "no message"}")
			}
		}
	}
	install(JsonFeature)
}

@Suppress("ConstantConditionIf")
private suspend fun usingScraper(block: suspend YTKtScraperCache.() -> Unit) = if (LIVE_TEST) withScraperCache(block)
	else withConfiguredScraperCacheClosing(genTestHTTPClient(), block)

@Ignore
@Test
fun testCCMetadataFromID() = platformRunBlocking {
	usingScraper {
//		VideoIDs.valid_existent_playable.forEach { // CBB
		VideoIDs.valid_existent_playable_withCCs.forEach {
			assertSucceeds { VideoID.parse(it)!!.getCCMetadata() }
		}
		VideoIDs.valid_nonexistent.forEach {
			assertFailsWith(VideoUnavailableException::class) { VideoID.parse(it)!!.getCCMetadata() }
		}
	}
}

@Ignore
@RequiresXMLParser
@Test
fun testCCs() = platformRunBlocking {
	usingScraper {
		VideoIDs.valid_existent_playable_withCCs.forEach {
			assertSucceeds {
				VideoID.parse(it)!!.getCCMetadata().random().getCaptions()
			}
		}
	}
}

@Ignore
@RequiresHTMLParser
@Test
fun testChannelFromID() = platformRunBlocking {
	usingScraper {
		ChannelIDs.valid.map { ChannelID.parse(it)!! }.forEach {
			assertSucceeds { it.getData() }
		}
	}
}

@Ignore
@RequiresHTMLParser
@Test
fun testChannelIDFromUsername() = platformRunBlocking {
	usingScraper {
		Usernames.valid.map { Username.parse(it)!! }.forEach {
			assertSucceeds { it.getChannelID() }
		}
	}
}

@Ignore
@Test
fun testPlaylistFromID() = platformRunBlocking {
	usingScraper {
		PlaylistData.rawResults.forEach { (rawID, data) ->
			assertTrue { PlaylistID.parse(rawID)!!.getData().videos.size == data.first }
		}
		PlaylistIDs.valid_unusable.map { PlaylistID.parse(it)!! }.forEach {
			assertFailsWith(ClientRequestException::class) { it.getData() }
		}
	}
}

@Ignore
@Test
fun testSearchResults() = platformRunBlocking {
	usingScraper {
		SearchResults.queries.forEach {
			assertTrue { it.getPartialResults(30).size <= 30 }
		}
	}
}

@Ignore
@RequiresHTMLParser
@RequiresXMLParser
@Test
fun testStreaming() = platformRunBlocking {
	usingScraper {
		VideoIDs.valid_nonexistent.map { VideoID.parse(it)!! }.forEach {
			assertFailsWith(VideoUnavailableException::class) { it.getStreamMetadataSet() }
		}
		VideoIDs.valid_existent_unplayable.map { VideoID.parse(it)!! }.forEach {
			assertFailsWith(VideoUnplayableException::class) { it.getStreamMetadataSet() } // the only datum is a premium video, so we should catch a VideoRequiresPurchaseException
		}
		assertSucceeds {
			VideoIDs.valid_existent_playable.map { VideoID.parse(it)!! }.forEach { videoID ->
				videoID.getStreamMetadataSet().audioVisual.minByOrNull { it.size }!!
			}
		}
	}
}

@Ignore
@RequiresHTMLParser
@Test
fun testVideoFromID() = platformRunBlocking {
	usingScraper {
		VideoIDs.valid_existent.map { VideoID.parse(it)!! }.forEach {
			assertSucceeds { it.getData() }
		}
		VideoIDs.valid_nonexistent.map { VideoID.parse(it)!! }.forEach {
			assertFailsWith(VideoUnavailableException::class) { it.getData() }
		}
	}
}
