package dev.yoshirulz.ytkt

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.runBlocking
import kotlin.reflect.KClass
import kotlin.test.fail
import kotlin.test.assertFailsWith as stdlibAssertFailsWith
import kotlin.test.assertTrue as stdlibAssertTrue

fun <T> platformRunBlocking(block: suspend CoroutineScope.() -> T) = runBlocking(block = block)

fun <T: Throwable> assertFailsWith(exceptionClass: KClass<T>, block: suspend CoroutineScope.() -> Unit) {
	stdlibAssertFailsWith(exceptionClass) { platformRunBlocking(block = block) }
}

fun assertSucceeds(block: suspend CoroutineScope.() -> Any?) {
	try {
		platformRunBlocking(block = block)
	} catch (e: Exception) {
		fail(e.message)
	}
}

fun assertTrue(block: suspend CoroutineScope.() -> Boolean) = stdlibAssertTrue(platformRunBlocking(block = block))
