package dev.yoshirulz.ytkt

import kotlinx.cinterop.alloc
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.ptr
import kotlinx.cinterop.toKString
import platform.posix.*

public actual fun nativeUAFragment(): String = memScoped {
	val osInfo = alloc<utsname>().also { uname(it.ptr) }
	"${osInfo.sysname.toKString()}/${osInfo.release.toKString()}; ${osInfo.machine.toKString()}"
}

public actual fun platformTimeFromYMD(year: Int, month: Int, day: Int): Timestamp = platformTimeNow() //TODO this isn't even close to correct

public actual fun platformTimeNow(): Timestamp = memScoped {
	val osInfo = alloc<timespec>()
	clock_gettime(CLOCK_REALTIME, osInfo.ptr)
	Timestamp(1000L * osInfo.tv_sec + osInfo.tv_nsec / 1000000L)
}
