package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.EntryPoint
import dev.yoshirulz.ytkt.URI

public data class User(val name: Username) {
	/** The user home page. */
	@EntryPoint
	val canonicalURI: URI get() = name.canonicalURI

	override fun toString(): String = "[user] $name"
}
