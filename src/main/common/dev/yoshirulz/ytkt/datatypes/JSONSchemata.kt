@file:Suppress("SpellCheckingInspection")

package dev.yoshirulz.ytkt.datatypes

// All these class' names were guessed based on their usages. This is not an official API.

public expect class YTAdaptiveStreamFormat {
	public val itag: Double
	public val url: String
	public val mimeType: String
	public val bitrate: Double
	public val width: Double?
	public val height: Double?
	public val contentLength: String?
	public val fps: Double?
	public val qualityLabel: String?
	public val audioSampleRate: String?
	public val cipher: String
}

public expect class YTCCTrack {
	public val baseUrl: String
	public val name: YTRichText
	public val vssId: String
	public val languageCode: String
}

public expect class YTCCTrackDetails {
	public val captionTracks: Array<YTCCTrack>?
}

public expect class YTCCTrackDetailsContainer {
	public val playerCaptionsTracklistRenderer: YTCCTrackDetails
}

public expect class YTEmbedPagePlayerConfig {
	public val assets: YTVideoPlayerAssets
}

public expect class YTPlayerResponse {
	public val captions: YTCCTrackDetailsContainer?
	public val playabilityStatus: YTPlayerResponsePlayabilityStatus?
	public val streamingData: YTPlayerResponseStreamingData?
	public val videoDetails: YTPlayerResponseVideoDetails?
}

public expect class YTPlayerResponseErrorScreenDetails {
	public val playerLegacyDesktopYpcTrailerRenderer: YTPlayerResponseLegacyPremiumTrailerDetails?
	public val ypcTrailerRenderer: YTPlayerResponsePremiumTrailerDetails?
}

public expect class YTPlayerResponseLegacyPremiumTrailerDetails {
	public val trailerVideoId: String
}

public expect class YTPlayerResponsePlayabilityStatus {
	public val status: String?
	public val reason: String?
	public val errorScreen: YTPlayerResponseErrorScreenDetails?
}

public expect class YTPlayerResponsePremiumTrailerDetails {
	public val playerVars: String
}

public expect class YTPlayerResponseStreamingData {
	public val expiresInSeconds: Double
	public val formats: Array<YTStreamFormat>?
	public val adaptiveFormats: Array<YTAdaptiveStreamFormat>?
	public val dashManifestUrl: String?
	public val hlsManifestUrl: String?
}

public expect class YTPlayerResponseVideoDetails {
	public val author: String
	public val viewCount: Double?
	public val title: String
	public val shortDescription: String
	public val isLive: Boolean?
	public val lengthSeconds: Double
	public val keywords: Array<String>?
}

public expect class YTPlaylistDetails {
	public val title: String
	public val description: String?
	/** system playlists have no views */
	public val views: Double?
	public val video: Array<YTVideoDetails>?
	/** system playlists have no author */
	public val author: String?
}

public expect class YTRichText {
	public val simpleText: String
}

public expect class YTSearchResults {
	public val video: Array<YTVideoDetails>
}

public expect class YTStreamFormat {
	public val itag: Double
	public val url: String
	public val mimeType: String
	public val contentLength: String?
	public val cipher: String
}

public expect class YTVideoDetails {
	@Suppress("PropertyName")
	public val time_created: Double
	public val description: String
	public val dislikes: Double
	public val title: String
	public val author: String
	public val keywords: String
	@Suppress("PropertyName")
	public val length_seconds: Double
	public val views: String
	/** YT video IDs are encrypted? Or were and the JSON key remains for backwards compatibility? */
	@Suppress("PropertyName")
	public val encrypted_id: String
	public val likes: Double
}

public expect class YTVideoPlayerAssets {
	public val js: String
}

public expect class YTWatchPagePlayerArgs {
	@Suppress("PropertyName")
	public val player_response: String
	@Suppress("PropertyName")
	public val url_encoded_fmt_stream_map: String
	@Suppress("PropertyName")
	public val adaptive_fmts: String
}

public expect class YTWatchPagePlayerConfig {
	public val args: YTWatchPagePlayerArgs
	public val assets: YTVideoPlayerAssets
}
