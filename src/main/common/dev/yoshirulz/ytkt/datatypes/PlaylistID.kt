package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.*
import io.ktor.http.*

public data class PlaylistID(val raw: String, val type: PlaylistType) {
	/** The playlist page. */
	val canonicalURI: URI get() = ytCanonicalURI("/playlist", parametersOf("list", raw))

	override fun toString(): String = raw

	public companion object {
		public enum class PlaylistType(public val prefix: String) {
			/** A channel's "Favorites" playlist (probably abbreviates "favorites list"). */
			ChannelFavorites("FL"),
			/** A channel's "Liked videos" playlist (probably abbreviates "liked list"). */
			ChannelLiked("LL"),
			/** A channel's "Your Likes" playlist (same as the likes playlist but filtered to only music, probably abbreviates "liked music"). */
			@Suppress("unused")
			ChannelLikedMusic("LM"),
			/**
			 * An autogenerated playlist populated by an artist's songs from one album, in order (a backronym is "opus list").
			 *
			 * These commonly use the wrong/live version of a song, or load from an unaffiliated channel instead of an "Official Artist Channel" or an autogenerated placeholder channel.
			 * The prefixes seem to actually be `OLAK5uy_k` through `OLAK5uy_n` (maybe it's simply incrementing?).
			 */
			@Suppress("unused")
			Album("OL"),
			/**
			 * A regular, user-generated playlist.
			 *
			 * Apart from the common 32-char alphanumeric IDs, there seem to be shorter (perhaps older?) 16-char hexadecimal IDs.
			 */
			Playlist("PL"),
			/** Like [ChannelUploads], but sorted by popularity instead of upload time. */
			PopularUploads("PU"),
			/**
			 * An autogenerated "YouTube Mix" playlist, populated with similar videos (probably abbreviates "random").
			 * YouTube Music introduced a special playlist ID for "My Supermix", `RDTMAK5uy_kset8DisdE7LSD4TNjEVvrKRTmG7a56sY`, which is identical for every user.
			 *
			 * IDs seem to be the "seed" video's ID prefixed by either `RD` or `RDMM`.
			 */
			YouTubeMix("RD"),
			/** Like [YouTubeMix], but limited to the same channel as this video (probably abbreviates "user list"). */
			SameChannelMix("UL"),
			/** An autogenerated playlist of all a channel's uploads (probably abbreviates "user uploads"). */
			ChannelUploads("UU"),
			/** A user's "Watch later" playlist. */
			WatchLater("WL");

			public companion object {
				public fun parse(prefix: String): PlaylistType = values().first { it.prefix == prefix }
			}
		}

		/** matches `PLOU2XLYxmsIJGErt5rrCqaSGTMyyqNt2H` and `WL` */
		private val PLAYLIST_ID_PATTERN = Regex("""^(?:PL(?:[\-0-9A-Z_a-z]{10}[\-0-9A-Z_a-z]{22}|[0-9A-F]{16})|OLAK5uy_[\-0-9A-Z_a-z]{33}|WL|(?:RDMM|RD|UL)${VideoID.VIDEO_ID_PATTERN_FRAGMENT}|RDTMAK5uy_kset8DisdE7LSD4TNjEVvrKRTmG7a56sY|(?:FL|LL|PU|UU)${ChannelID.CHANNEL_ID_PATTERN_FRAGMENT}|LL|LM)$""")

		@EntryPoint
		public val CURRENT_USER_WATCH_LATER: PlaylistID = PlaylistID(PlaylistType.WatchLater.prefix, PlaylistType.WatchLater)

		public fun parse(raw: String): PlaylistID? = if (PLAYLIST_ID_PATTERN matches raw) PlaylistID(raw, PlaylistType.parse(raw.substring(0..1))) else null

		@EntryPoint
		public fun parseFromURI(uri: String): PlaylistID? = parseFromURI(uri.parseURI())

		public fun parseFromURI(uri: URI): PlaylistID? = when (YouTubeDomain.ofURI(uri)) {
			YouTubeDomain.MainOrMirror -> uri.ktorUrl.parameters["list"]?.takeIf {
				uri.ktorUrl.encodedPath.let { it.startsWith("/embed/") || it == "/playlist" || it == "/watch" }
			}
			YouTubeDomain.Short -> uri.ktorUrl.parameters["list"]
			else -> null
		}?.let(::parse)
	}
}
