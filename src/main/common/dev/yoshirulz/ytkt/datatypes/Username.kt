package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.*

public data class Username internal constructor(val raw: String) {
	/** The user home page. */
	val canonicalURI: URI get() = ytCanonicalURI("/user/$raw")

	override fun toString(): String = raw

	public companion object {
		/** matches `dwangoAC` */
		private val USERNAME_PATTERN = Regex("^[0-9A-Za-z]{1,20}\$")

		public fun parse(raw: String): Username? = if (USERNAME_PATTERN matches raw) Username(raw) else null

		@EntryPoint
		public fun parseFromURI(uri: String): Username? = parseFromURI(uri.parseURI()) //TODO inline these?

		public fun parseFromURI(uri: URI): Username? = uri.ktorUrl.encodedPath.let {
			if (YouTubeDomain.ofURI(uri) == YouTubeDomain.MainOrMirror && it.startsWith("/user/"))
				parse(it.substring(6).substringBefore('/'))
			else null
		}
	}
}
