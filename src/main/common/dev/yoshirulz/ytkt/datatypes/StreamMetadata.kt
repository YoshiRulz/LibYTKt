package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.EntryPoint
import dev.yoshirulz.ytkt.Timestamp
import dev.yoshirulz.ytkt.URI

public typealias AdaptiveStreamMetadataSet = Pair<List<AudioOnlyStreamMetadata>, List<VideoOnlyStreamMetadata>>

public val GET_BITRATE_FROM_TEST_DATA: ULong = 0UL //TODO

public val GET_FRAMERATE_FROM_TEST_DATA: UInt = 0U //TODO

public interface StreamMetadata {
	public val iTag: ITag
	public val uri: URI
	public val container: Container
	/** In bytes (B). */ public val size: ULong
	/** In bits per second (b/s). */ public val bitrate: ULong
}

public interface AudioStreamMetadata: StreamMetadata {
	public val audioEncoding: AudioEncoding
}

public interface VideoStreamMetadata: StreamMetadata {
	public val videoEncoding: VideoEncoding
	public val videoQuality: VideoQuality
	public val resolution: VideoResolution
	public val framerate: UInt
}

public class AudioOnlyStreamMetadata(
	override val iTag: ITag,
	override val uri: URI,
	override val container: Container,
	override val size: ULong,
	override val bitrate: ULong,
	override val audioEncoding: AudioEncoding
): AudioStreamMetadata {
	override fun toString(): String = "$iTag ($container) [audio]"
}

public class VideoOnlyStreamMetadata(
	override val iTag: ITag,
	override val uri: URI,
	override val container: Container,
	override val size: ULong,
	override val bitrate: ULong,
	override val videoEncoding: VideoEncoding,
	override val videoQuality: VideoQuality,
	override val resolution: VideoResolution,
	override val framerate: UInt
): VideoStreamMetadata {
	override fun toString(): String = "$iTag ($container) [video]"
}

public class AudioVisualStreamMetadata(
	override val iTag: ITag,
	override val uri: URI,
	override val container: Container,
	override val size: ULong,
	override val bitrate: ULong,
	override val audioEncoding: AudioEncoding,
	override val videoEncoding: VideoEncoding,
	override val videoQuality: VideoQuality,
	override val resolution: VideoResolution,
	override val framerate: UInt
): AudioStreamMetadata, VideoStreamMetadata {
	override fun toString(): String = "$iTag ($container) [av]"
}

public data class StreamMetadataSet(
	val audioVisual: List<AudioVisualStreamMetadata>,
	val audioOnly: List<AudioOnlyStreamMetadata>,
	val videoOnly: List<VideoOnlyStreamMetadata>,
	/** Expiry date for this information */ val validUntil: Timestamp,
	/** Raw HTTP Live Streaming (HLS) URL to the m3u8 playlist. Null if not a live stream. */ val hlsLiveStreamUrl: String?
) {
	val all: List<StreamMetadata> by lazy { audioVisual + audioOnly + videoOnly }

	@EntryPoint
	val allWithAudio: List<AudioStreamMetadata> by lazy { audioVisual + audioOnly }

	@EntryPoint
	val allWithVideo: List<VideoStreamMetadata> by lazy { audioVisual + videoOnly }
}
