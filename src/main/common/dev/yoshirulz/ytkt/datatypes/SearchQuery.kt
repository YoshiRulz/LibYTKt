package dev.yoshirulz.ytkt.datatypes

public data class SearchQuery(val query: String)
