package dev.yoshirulz.ytkt

import dev.yoshirulz.ytkt.datatypes.StreamMetadataSet
import dev.yoshirulz.ytkt.datatypes.VideoID

/**
 * Most use cases will only need one instance of this during their execution lifecycle.
 * @see withScraperCache
 */
public expect class YTKtScraperCache {
	public suspend inline fun VideoID.getStreamMetadataSet(): StreamMetadataSet

	public companion object {
		public fun createDefaultInstance(): YTKtScraperCache
	}
}
