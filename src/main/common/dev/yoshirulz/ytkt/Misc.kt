package dev.yoshirulz.ytkt

import io.ktor.http.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json

public typealias JSONString = String

public const val EMPTY_STRING: String = ""

internal enum class YouTubeDomain {
	Invalid, MainOrMirror, Short;

	companion object {
		const val CANONICAL_DOMAIN = "www.youtube.com"

		@Suppress("SpellCheckingInspection")
		const val SHORT_DOMAIN = "youtu.be"

		private val MIRRORS = arrayOf("youtube.com", "youtube.de")

		fun ofURI(uri: URI): YouTubeDomain = uri.ktorUrl.host.let { domain ->
			when {
				domain == SHORT_DOMAIN -> Short
				domain.removePrefix("www.") in MIRRORS -> MainOrMirror
				domain.removePrefix("m.") in MIRRORS -> MainOrMirror
				else -> Invalid
			}
		}
	}
}

public class URI(@Suppress("SpellCheckingInspection") public val ktorUrl: Url) {
	override fun toString(): String = ktorUrl.toString()
}

/** Creates a [YTKtScraperCache] with all defaults, and passes it to the block (as a receiver). */
public suspend fun <T> withScraperCache(block: suspend YTKtScraperCache.() -> T): T = block(YTKtScraperCache.createDefaultInstance())

internal fun ytCanonicalURI(path: String, parameters: Parameters = Parameters.Empty, isShortened: Boolean = false) = URI(Url(
	URLProtocol.HTTPS,
	if (isShortened) YouTubeDomain.SHORT_DOMAIN else YouTubeDomain.CANONICAL_DOMAIN,
	0,
	path,
	parameters,
	EMPTY_STRING,
	null,
	null,
	false
))

public fun <T> JSONString.parsedWith(kSer: KSerializer<T>): T = Json.decodeFromString(kSer, this)

public fun Parameters.copyAndSet(key: String, value: String): Parameters
	= if (this[key] == null) this + parametersOf(key, value)
		else parametersOf(
			*this.entries()
				.mapTo(mutableListOf()) { it.toPair() }
				.also {
					it.remove(it.first { (k, _) -> k == key })
					it.add(Pair(key, listOf(value)))
				}
				.toTypedArray()
		)

public fun String.parseURI(): URI = URI(when {
	this.isBlank() -> throw UnrecognisedURIException(this, "Blank URI")
	this.startsWith("https://") -> Url(this)
	this.startsWith("http://") -> throw UnsecuredURIException(this)
	this.contains("://") -> throw UnrecognisedURIException(this)
	else -> Url("https://${this}")
})

/** "path-encoded" referring to URIs such as `example.com/api/endpoint/keyA/valueA/keyB/valueB` */
public fun String.setURIPathEncParam(key: String, value: String): String
	= Regex("""/${Regex.escape(key)}/([^/]+)(?:/|$)""").find(this)?.let { match ->
		this.replace(match.groupValues[0], value) // if the param was found, replace it
	} ?: "$this/$key/$value" // else, it wasn't present, so append it

public fun String.stripNonDigit(): String = this.replace(Regex("\\D"), EMPTY_STRING)

public fun String.swapCharsAt(firstIndex: Int, secondIndex: Int): String
	= if (firstIndex < secondIndex) "${this.substring(0, firstIndex)}${this[secondIndex]}${this.substring(firstIndex + 1, secondIndex)}${this[firstIndex]}${this.substring(secondIndex + 1)}"
		else this.swapCharsAt(secondIndex, firstIndex)

public fun URI.copyAndSetParam(key: String, value: String): URI = URI(this.ktorUrl.copy(parameters = this.ktorUrl.parameters.copyAndSet(key, value)))
