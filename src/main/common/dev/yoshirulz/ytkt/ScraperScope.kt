package dev.yoshirulz.ytkt

import kotlinx.coroutines.CoroutineScope

public class ScraperScope internal constructor(private val parentScope: CoroutineScope, private val scraperCache: YTKtScraperCache): CoroutineScope by parentScope {
	public companion object
}

public fun <R> CoroutineScope.withScraper(scraperCache: YTKtScraperCache, block: ScraperScope.() -> R): R = block(ScraperScope(this, scraperCache))
