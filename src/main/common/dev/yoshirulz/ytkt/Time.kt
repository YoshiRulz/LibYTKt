package dev.yoshirulz.ytkt

import kotlin.time.Duration

@Suppress("SpellCheckingInspection")
public typealias RelativeTimespan = LongRange

public inline class RelativeTimestamp(/** Milliseconds since an arbitrary epoch i.e. the start of a video */ public val ms: Long) {
	public operator fun rangeTo(other: RelativeTimestamp): RelativeTimespan = ms..other.ms

	override fun toString(): String {
		fun Long.pad(i: Int = 2) = this.toString().padStart(i, '0')
		val s = ms / 1000L
		val m = s / 60L
		val h = m / 60L
		return "${h.pad()}:${(m - h * 60L).pad()}:${(s - m * 60L).pad()}.${(ms - s * 1000L).pad(3)}"
	}
}

public inline class Timestamp(/** Milliseconds since Unix epoch */ private val ms: Long) {
	public operator fun plus(other: Duration): Timestamp = Timestamp(ms + other.toLongMilliseconds())

	public companion object {
		public fun fromYMDString(str: String): Timestamp = str.split('-', limit = 3).let { (y, m, d) -> platformTimeFromYMD(y.toInt(), m.toInt(), d.toInt()) }

		public fun now(): Timestamp = platformTimeNow()
	}
}

public expect fun platformTimeFromYMD(year: Int, month: Int, day: Int): Timestamp

public expect fun platformTimeNow(): Timestamp
