package dev.yoshirulz.ytkt

import dev.yoshirulz.ytkt.datatypes.VideoID

public class PageRequestFailureException: Exception("GET request failed or could not parse HTML from response body")

/** Thrown when certain essential information can't be extracted, usually because YouTube changed some internals somewhere */
public class UnrecognisedStructureException(failedOn: String): Exception("Could not find signature decipherer function $failedOn.")

/** Thrown by [parseURI] when the receiver is a blank string or specifies a protocol that is not HTTP(S) */
public class UnrecognisedURIException(uri: String, message: String = "Unknown URI protocol: $uri"): URIParseException(uri, message)

/** Thrown by [parseURI] when the receiver specifies HTTP */
public class UnsecuredURIException(uri: String): URIParseException(uri, "Use HTTPS: $uri")

public open class URIParseException(public val uri: String, message: String): Exception(message)

/** Thrown when a video is not playable because it requires purchase */
public class VideoRequiresPurchaseException(id: VideoID, @Suppress("CanBeParameter") public val previewVideoID: String): VideoUnplayableException(id, "Video [$id] is unplayable because it requires purchase (the response included a preview video at ID $previewVideoID).")

/**
 * Thrown when a video is not available and cannot be processed.
 * This can happen because the video does not exist, is deleted, is private, or due to other reasons.
 */
public class VideoUnavailableException(id: VideoID): VideoUnplayableException(id, "The video is deleted, set to private, or otherwise unavailable.")

/**
 * Thrown when a video is not playable and its streams cannot be resolved.
 * This can happen because the video requires purchase, is blocked in your country, is controversial, or due to other reasons.
 */
public open class VideoUnplayableException(public val id: VideoID, reason: String? = null): Exception("Video [$id] is unplayable. Reason: ${reason ?: "none"}")
