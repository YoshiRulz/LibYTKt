package dev.yoshirulz.ytkt

import dev.yoshirulz.simple_http.sendGETForPlaintext
import io.ktor.client.*
import kotlinx.serialization.KSerializer

/** waiting on [Kotlin/kotlinx.serialization#188](https://github.com/Kotlin/kotlinx.serialization/issues/188), one of the contributors has made a separate library but it doesn't target Kotlin/Native */
@RequiresOptIn
@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.FUNCTION)
internal annotation class RequiresHTMLParser

/** @see RequiresHTMLParser */
@RequiresOptIn
@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.FUNCTION)
internal annotation class RequiresXMLParser

internal suspend inline fun <T: Any> sendGETForJSON(uri: String, kSer: KSerializer<T>, httpClient: HttpClient)
	= sendGETForPlaintext(uri, httpClient).parsedWith(kSer)

/** Creates a [YTKtScraperCache] using [httpClient], and passes it to the block (as a receiver). The [httpClient] will **not** be closed by this function. */
public suspend fun <T> withConfiguredScraperCache(httpClient: HttpClient, block: suspend YTKtScraperCache.() -> T): T = block(YTKtScraperCache(httpClient))

/** Creates a [YTKtScraperCache] using [httpClient], and passes it to the block (as a receiver). The [httpClient] will be closed. */
public suspend fun <T> withConfiguredScraperCacheClosing(httpClient: HttpClient, block: suspend YTKtScraperCache.() -> T): T = block(YTKtScraperCache(httpClient)).also { httpClient.close() }

@Suppress("RedundantSuspendModifier", "unused", "UNUSED_PARAMETER")
internal suspend fun HttpClient.getContentLength(requestUri: URI) = 0UL as ULong? //TODO return content length from GET (HEADER?) response headers, null if response status code isn't 2xx

internal suspend inline fun <T: Any> URI.sendGETForJSON(kSer: KSerializer<T>, httpClient: HttpClient)
	= this.sendGETForPlaintext(httpClient).parsedWith(kSer)

public expect fun nativeUAFragment(): String
