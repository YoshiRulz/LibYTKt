package dev.yoshirulz.ytkt

import dev.yoshirulz.ytkt.datatypes.*
import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import kotlinx.coroutines.channels.ReceiveChannel

public actual class YTKtScraperCache(public val httpClient: HttpClient) {
	public val cipherCache: YTKtCipherCache = YTKtCipherCache(httpClient)

	@RequiresXMLParser
	public suspend inline fun CCTrackMetadata.getCaptions(): ClosedCaptionsTrack = this.getCaptions(this@YTKtScraperCache.httpClient)

	@RequiresHTMLParser
	public suspend inline fun ChannelID.getData(): Channel = this.getData(this@YTKtScraperCache.httpClient)

	public suspend inline fun PlaylistID.getData(): Playlist = this.getData(this@YTKtScraperCache.httpClient)

	public suspend inline fun SearchQuery.getPartialResults(maxResults: Int): List<Video> = this.getPartialResults(this@YTKtScraperCache.httpClient, maxResults)

	@Suppress("NOTHING_TO_INLINE")
	public inline fun SearchQuery.getResults(): ReceiveChannel<Video> = this.getResults(this@YTKtScraperCache.httpClient)

	@RequiresHTMLParser
	public suspend inline fun Username.getChannelID(): ChannelID = this.getChannelID(this@YTKtScraperCache.httpClient)

	public suspend inline fun Username.getData(): User = this.getData(this@YTKtScraperCache.httpClient)

	public suspend inline fun VideoID.getCCMetadata(): List<CCTrackMetadata> = this.getCCMetadata(this@YTKtScraperCache.httpClient)

	@RequiresHTMLParser
	public suspend inline fun VideoID.getData(): Video = this.getData(this@YTKtScraperCache.httpClient)

	@RequiresHTMLParser
	@RequiresXMLParser
	public actual suspend inline fun VideoID.getStreamMetadataSet(): StreamMetadataSet = this.getStreamMetadataSet(this@YTKtScraperCache.httpClient, this@YTKtScraperCache.cipherCache)

	public actual companion object {
		public actual fun createDefaultInstance(): YTKtScraperCache = YTKtScraperCache(HttpClient {
			install(JsonFeature)
			install(UserAgent) {
				agent = "curl/7.63.0 (${nativeUAFragment()})"
			}
		})
	}
}
