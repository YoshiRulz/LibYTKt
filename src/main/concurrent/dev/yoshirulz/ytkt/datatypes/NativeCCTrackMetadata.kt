package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.simple_http.root
import dev.yoshirulz.simple_http.sendGETForXML
import dev.yoshirulz.ytkt.RelativeTimestamp
import dev.yoshirulz.ytkt.RequiresXMLParser
import io.ktor.client.*

@RequiresXMLParser
public suspend fun CCTrackMetadata.getCaptions(httpClient: HttpClient): ClosedCaptionsTrack = this.uri.sendGETForXML(httpClient).root
	.descendants("p")
	.associateWith { it.toString() }
	.filterNot { (_, s) -> s.isEmpty() }
	.map { (e, s) ->
		val startTime = e.attribute("t").toLong()
		CCEntry(s, RelativeTimestamp(startTime)..RelativeTimestamp(startTime + e.attribute("d").toLong()))
	}
