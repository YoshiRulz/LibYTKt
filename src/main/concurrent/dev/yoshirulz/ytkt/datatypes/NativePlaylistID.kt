package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.sendGETForJSON
import dev.yoshirulz.ytkt.ytCanonicalURI
import io.ktor.client.*
import io.ktor.http.*
import kotlin.collections.set

/**
 * TODO inherited docs claim 1 "page" holds <= 200 videos, but the index is incremented by 100...
 * TODO test playlist visibility options
 */
public suspend fun PlaylistID.getData(httpClient: HttpClient): Playlist {
	suspend /*inline*/ fun getRawResults(index: UInt) = ytCanonicalURI(
		"/list_ajax",
		parametersOf(
			"style" to listOf("json"),
			"action_get_list" to listOf("1"),
			"list" to listOf(this.raw),
			"index" to listOf(index.toString()),
			"hl" to listOf("en")
		)
	).sendGETForJSON(YTPlaylistDetails.kSer, httpClient)
	/*inline*/ fun appendFromRaw(received: MutableMap<VideoID, Video>, rawResults: YTPlaylistDetails): UInt {
		val raw = rawResults.video?.takeIf { it.isNotEmpty() } ?: return 0U
		var added = 0U
		var cachedID: VideoID
		raw.forEach { videoJSON ->
			cachedID = VideoID.parse(videoJSON.encrypted_id)!!
			if (!received.containsKey(cachedID)) {
				received[cachedID] = Video.fromJSON(videoJSON, cachedID)
				added++
			}
		}
		return added
	}
	var index = if (this.type == PlaylistID.Companion.PlaylistType.Playlist) 101U else 1U
	val firstRaw = getRawResults(index)
	val first = firstRaw.video.orEmpty()
	val received = mutableMapOf<VideoID, Video>()
	if (first.isNotEmpty()) {
		first.forEach { received[VideoID.parse(it.encrypted_id)!!] = Video.fromJSON(it) } // first "page" will never contain duplicates
		do index += 100U while (appendFromRaw(received, getRawResults(index)) != 0U)
	}
	return Playlist(
		this,
		firstRaw.author.orEmpty(),
		firstRaw.title,
		firstRaw.description.orEmpty(),
		firstRaw.views?.toULong() ?: 0UL,
		received.values.toList()
	)
}
