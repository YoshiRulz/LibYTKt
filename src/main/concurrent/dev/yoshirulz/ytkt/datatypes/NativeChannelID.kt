package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.simple_http.querySelector
import dev.yoshirulz.simple_http.sendGETForHTML
import dev.yoshirulz.ytkt.RequiresHTMLParser
import io.ktor.client.*

@RequiresHTMLParser
public suspend fun ChannelID.getData(httpClient: HttpClient): Channel = canonicalURI.sendGETForHTML(httpClient).let { channelPageBody ->
	Channel(
		this,
		channelPageBody.querySelector("""meta[property="og:title"]""")!!.getAttribute("content")!!,
		channelPageBody.querySelector("""meta[property="og:image"]""")!!.getAttribute("content")!!
	)
}
