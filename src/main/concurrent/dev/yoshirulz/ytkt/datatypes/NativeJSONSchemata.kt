@file:Suppress("SpellCheckingInspection", "unused")

package dev.yoshirulz.ytkt.datatypes

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable

@Serializable
public actual class YTAdaptiveStreamFormat(
	public actual val itag: Double,
	public actual val url: String,
	public actual val mimeType: String,
	public actual val bitrate: Double,
	public actual val width: Double? = null,
	public actual val height: Double? = null,
	public val initRange: YTRange,
	public val indexRange: YTRange,
	public val lastModified: String,
	public actual val contentLength: String? = null,
	public val quality: String,
	public actual val fps: Double? = null,
	public actual val qualityLabel: String? = null,
	public val projectionType: String,
	public val averageBitrate: Double,
	public val colorInfo: YTColourInfo? = null,
	public val highReplication: Boolean? = null,
	public val audioQuality: String? = null,
	public val approxDurationMs: String,
	public actual val audioSampleRate: String? = null,
	public val audioChannels: Double? = null,
	public actual val cipher: String
)

@Serializable
public class YTAdSafetyReason(
	public val apmUserPreference: YTAPMUserPreference,
	public val isWatchHistoryPaused: Boolean? = null,
	public val isRelevantAdsOptout: Boolean? = null,
	public val isEmbed: Boolean,
	public val isFocEnabled: Boolean? = null,
	public val isSearchHistoryPaused: Boolean? = null
)

@Serializable
public class YTAnnotationDetails(public val playerAnnotationsUrlsRenderer: YTPlayerAnnotationsUrlsRenderer)

@Serializable
public class YTAPMUserPreference(public val dummy: Unit = Unit)

@Serializable
public class YTCCAudioTrack(public val captionTrackIndices: Array<Double>, public val defaultCaptionTrackIndex: Double, public val visibility: String, public val hasDefaultTrack: Boolean)

@Serializable
public class YTCCLanguage(public val languageCode: String, public val languageName: YTRichText)

@Serializable
public actual class YTCCTrack(
	public actual val baseUrl: String,
	public actual val name: YTRichText,
	public actual val vssId: String,
	public actual val languageCode: String,
	public val rtl: Boolean? = null,
	public val isTranslatable: Boolean,
	public val kind: String? = null
)

@Serializable
public actual class YTCCTrackDetails(
	public actual val captionTracks: Array<YTCCTrack>? = null,
	public val audioTracks: Array<YTCCAudioTrack>? = null,
	public val translationLanguages: Array<YTCCLanguage>? = null,
	public val defaultAudioTrackIndex: Double? = null
)

@Serializable
public actual class YTCCTrackDetailsContainer(public actual val playerCaptionsTracklistRenderer: YTCCTrackDetails)

@Serializable
public class YTColourInfo(public val primaries: String, public val transferCharacteristics: String, public val matrixCoefficients: String)

@Serializable
public class YTEmbedPagePlayerArgs(
	public val vss_host: String,
	public val enablecastapi: String,
	public val adformat: String? = null, //TODO only seen as null, String is a guess
	public val el: String,
	public val cver: String,
	public val cbrver: String,
	public val view_count: Double,
	public val origin: String,
	public val authuser: Byte,
	public val gapi_hint_params: String,
	public val profile_picture: String,
	public val channel_path: String,
	public val user_display_name: String,
	public val rel: String,
	public val is_html5_mobile_device: Boolean,
	public val hl: String,
	public val fexp: String,
	public val video_id: String,
	public val c: String,
	public val expanded_subtitle: String,
	public val cbr: String,
	public val innertube_api_version: String,
	public val user_display_image: String,
	public val is_embed: String,
	public val length_seconds: Double,
	public val subtitle: String,
	public val embedded_player_response: String,
	public val cr: String,
	public val ssl: String,
	public val allow_embed: Byte,
	public val avg_rating: Double,
	public val title: String,
	public val cos: String,
	public val innertube_context_client_version: String,
	public val host_language: String,
	public val embed_config: String,
	public val expanded_title: String,
	public val fflags: String,
	public val allow_ratings: Byte,
	public val eventid: String,
	public val enablejsapi: String,
	public val showwatchlater: String,
	public val short_view_count_text: String,
	public val innertube_api_key: String
)

@Serializable
public actual class YTEmbedPagePlayerConfig(
	public val attrs: YTVideoPlayerAttrs,
	public val sts: Double,
	public val args: YTEmbedPagePlayerArgs,
	public actual val assets: YTVideoPlayerAssets
) {
	public companion object {
		public val kSer: KSerializer<YTEmbedPagePlayerConfig> = serializer()
	}
}

@Serializable
public class YTIconDetails(public val iconType: String)

@Serializable
public class YTMealbarPromoRendererBrowseEndpoint(public val browseId: String, public val params: String)

@Serializable
public class YTMealbarPromoRendererButtonDetails(public val buttonRenderer: YTMealbarPromoRendererButtonRendererDetails)

@Serializable
public class YTMealbarPromoRendererButtonRendererDetails(
	public val style: String,
	public val size: String,
	public val text: YTMealbarPromoRendererTextContainer,
	public val navigationEndpoint: YTMealbarPromoRendererEndpointDetails? = null,
	public val serviceEndpoint: YTMealbarPromoRendererEndpointDetails? = null,
	public val trackingParams: String
)

@Serializable
public class YTMealbarPromoRendererDetails(
	public val messageTexts: Array<YTMealbarPromoRendererTextContainer>,
	public val actionButton: YTMealbarPromoRendererButtonDetails,
	public val dismissButton: YTMealbarPromoRendererButtonDetails,
	public val triggerCondition: String,
	public val style: String,
	public val trackingParams: String,
	public val impressionEndpoints: Array<YTMealbarPromoRendererEndpointDetails>,
	public val isVisible: Boolean,
	public val messageTitle: YTMealbarPromoRendererTextContainer
)

@Serializable
public class YTMealbarPromoRendererEndpointDetails(public val clickTrackingParams: String, public val browseEndpoint: YTMealbarPromoRendererBrowseEndpoint? = null, public val feedbackEndpoint: YTMealbarPromoRendererFeedbackEndpoint? = null)

@Serializable
public class YTMealbarPromoRendererFeedbackEndpoint(public val feedbackToken: String, public val uiActions: YTMealbarPromoRendererUIActions)

@Serializable
public class YTMealbarPromoRendererRunText(public val text: String)

@Serializable
public class YTMealbarPromoRendererTextContainer(public val runs: Array<YTMealbarPromoRendererRunText>)

@Serializable
public class YTMealbarPromoRendererUIActions(public val hideEnclosingContainer: Boolean)

@Serializable
public class YTPlaybackTrackingDetails(
	public val videostatsPlaybackUrl: YTPlaybackTrackingURI,
	public val videostatsDelayplayUrl: YTPlaybackTrackingURI,
	public val videostatsWatchtimeUrl: YTPlaybackTrackingURI,
	public val ptrackingUrl: YTPlaybackTrackingURI,
	public val qoeUrl: YTPlaybackTrackingURI,
	public val setAwesomeUrl: YTPlaybackTrackingURI,
	public val atrUrl: YTPlaybackTrackingURI,
	public val youtubeRemarketingUrl: YTPlaybackTrackingURI
)

@Serializable
public class YTPlaybackTrackingURI(public val baseUrl: String, public val elapsedMediaTimeSeconds: Double? = null)

@Serializable
public class YTPlayerAdParams(public val enabledEngageTypes: String)

@Serializable
public class YTPlayerAdDetails(public val playerLegacyDesktopWatchAdsRenderer: YTPlayerLegacyDesktopWatchAdsRendererDetails)

@Serializable
public class YTPlayerAnnotationsUrlsRenderer(public val invideoUrl: String, public val loadPolicy: String, public val allowInPlaceSwitch: Boolean)

@Serializable
public class YTPlayerAttestationDetails(public val playerAttestationRenderer: YTPlayerAttestationRenderer)

@Serializable
public class YTPlayerAttestationRenderer(public val challenge: String)

@Serializable
public class YTPlayerAudioConfig(public val loudnessDb: Double, public val perceptualLoudnessDb: Double)

@Serializable
public class YTPlayerConfigDetails(public val audioConfig: YTPlayerAudioConfig, public val streamSelectionConfig: YTPlayerSelectionConfig, public val mediaCommonConfig: YTPlayerMediaConfig)

@Serializable
public class YTPlayerEndscreenDetails(public val endscreenUrlRenderer: YTPlayerEndscreenUrlRendererDetails)

@Serializable
public class YTPlayerEndscreenUrlRendererDetails(public val url: String)

@Serializable
public class YTPlayerLegacyDesktopWatchAdsRendererDetails(public val playerAdParams: YTPlayerAdParams)

@Serializable
public class YTPlayerMediaConfig(public val dynamicReadaheadConfig: YTPlayerReadaheadConfig)

@Serializable
public class YTPlayerReadaheadConfig(public val maxReadAheadMediaTimeMs: Double, public val minReadAheadMediaTimeMs: Double, public val readAheadGrowthRateMs: Double)

@Serializable
public actual class YTPlayerResponse(
	public actual val streamingData: YTPlayerResponseStreamingData? = null,
	public actual val playabilityStatus: YTPlayerResponsePlayabilityStatus? = null,
	public val playerAds: Array<YTPlayerAdDetails>? = null,
	public val playbackTracking: YTPlaybackTrackingDetails? = null,
	public actual val captions: YTCCTrackDetailsContainer? = null,
	public actual val videoDetails: YTPlayerResponseVideoDetails? = null,
	public val annotations: Array<YTAnnotationDetails>? = null,
	public val playerConfig: YTPlayerConfigDetails? = null,
	public val storyboards: YTPlayerStoryboardDetails? = null,
	public val trackingParams: String? = null,
	public val attestation: YTPlayerAttestationDetails? = null,
	public val messages: Array<YTPlayerResponseMessage>? = null,
	public val endscreen: YTPlayerEndscreenDetails? = null,
	public val adSafetyReason: YTAdSafetyReason? = null
) {
	public companion object {
		public val kSer: KSerializer<YTPlayerResponse> = serializer()
	}
}

@Serializable
public class YTPlayerResponseErrorMessageDetails(public val subreason: YTRichText, public val reason: YTRichText, public val thumbnail: YTThumbnailList, public val icon: YTIconDetails)

@Serializable
public actual class YTPlayerResponseErrorScreenDetails(
	public actual val playerLegacyDesktopYpcTrailerRenderer: YTPlayerResponseLegacyPremiumTrailerDetails? = null,
	public actual val ypcTrailerRenderer: YTPlayerResponsePremiumTrailerDetails? = null,
	public val playerErrorMessageRenderer: YTPlayerResponseErrorMessageDetails? = null
)

@Serializable
public actual class YTPlayerResponseLegacyPremiumTrailerDetails(public actual val trailerVideoId: String)

@Serializable
public class YTPlayerResponseMessage(public val mealbarPromoRenderer: YTMealbarPromoRendererDetails)

@Serializable
public actual class YTPlayerResponsePlayabilityStatus(public val playableInEmbed: Boolean? = null, public actual val status: String? = null, public actual val reason: String? = null, public actual val errorScreen: YTPlayerResponseErrorScreenDetails? = null)

@Serializable
public actual class YTPlayerResponsePremiumTrailerDetails(public actual val playerVars: String)

@Serializable
public actual class YTPlayerResponseStreamingData(
	public actual val expiresInSeconds: Double,
	public actual val formats: Array<YTStreamFormat>? = null,
	public actual val adaptiveFormats: Array<YTAdaptiveStreamFormat>? = null,
	public actual val dashManifestUrl: String? = null,
	public actual val hlsManifestUrl: String? = null
)

@Serializable
public actual class YTPlayerResponseVideoDetails(
	public actual val author: String,
	public actual val viewCount: Double? = null,
	public actual val title: String,
	public actual val shortDescription: String,
	public actual val isLive: Boolean? = null,
	public actual val lengthSeconds: Double,
	public actual val keywords: Array<String>? = null,
	public val channelId: String,
	public val videoId: String? = null,
	public val isOwnerViewing: Boolean? = null,
	public val isCrawlable: Boolean? = null,
	public val thumbnail: YTThumbnailList? = null,
	public val useCipher: Boolean? = null,
	public val averageRating: Double? = null,
	public val allowRatings: Boolean? = null,
	public val isPrivate: Boolean? = null,
	public val isUnpluggedCorpus: Boolean? = null,
	public val isLiveContent: Boolean? = null
)

@Serializable
public class YTPlayerSelectionConfig(public val maxBitrate: String)

@Serializable
public class YTPlayerStoryboardDetails(public val playerStoryboardSpecRenderer: YTPlayerStoryboardSpecRenderer)

@Serializable
public class YTPlayerStoryboardSpecRenderer(public val spec: String)

@Serializable
public actual class YTPlaylistDetails(
	public actual val title: String,
	public actual val description: String? = null,
	public actual val views: Double? = null,
	public actual val video: Array<YTVideoDetails>? = null,
	public actual val author: String? = null
) {
	public companion object {
		public val kSer: KSerializer<YTPlaylistDetails> = serializer()
	}
}

@Serializable
public class YTRange(public val start: Double, public val end: Double)

@Serializable
public actual class YTRichText(public actual val simpleText: String)

@Serializable
public actual class YTSearchResults(public val hits: Double, public actual val video: Array<YTVideoDetails>) {
	public companion object {
		public val kSer: KSerializer<YTSearchResults> = serializer()
	}
}

@Serializable
public actual class YTStreamFormat(
	public actual val itag: Double,
	public actual val url: String,
	public actual val mimeType: String,
	public val bitrate: Double,
	public val width: Double,
	public val height: Double,
	public val lastModified: String,
	public actual val contentLength: String? = null,
	public val quality: String,
	public val qualityLabel: String,
	public val projectionType: String,
	public val averageBitrate: Double? = null,
	public val audioQuality: String,
	public val approxDurationMs: String? = null,
	public val audioSampleRate: String? = null,
	public val audioChannels: Double? = null,
	public actual val cipher: String
)

@Serializable
public class YTThumbnailDetails(public val url: String, public val width: Double, public val height: Double)

@Serializable
public class YTThumbnailList(public val thumbnails: Array<YTThumbnailDetails>)

@Serializable
public actual class YTVideoDetails(
	public val privacy: String,
	public val user_id: String,
	public val duration: String,
	public val cc_license: Boolean,
	public val rating: Float,
	public val thumbnail: String,
	public val added: String,
	public actual val time_created: Double,
	public actual val description: String,
	public actual val dislikes: Double,
	public actual val title: String,
	public actual val author: String,
	public val is_hd: Boolean,
	public val comments: String,
	public actual val keywords: String,
	public actual val length_seconds: Double,
	public actual val views: String,
	public val is_cc: Boolean,
	public actual val encrypted_id: String,
	public val category_id: Byte,
	public actual val likes: Double,
	public val session_data: String? = null,
	public val endscreen_autoplay_session_data: String? = null
)

@Serializable
public actual class YTVideoPlayerAssets(public actual val js: String, public val css: String)

@Serializable
public class YTVideoPlayerAttrs(public val id: String, public val width: String? = null, public val height: String? = null)

@Serializable
public actual class YTWatchPagePlayerArgs(
	public val cbrver: String,
	public val innertube_api_version: String,
	public val video_id: String,
	public val host_language: String,
	public val csi_page_type: String,
	public val enablecsi: String,
	public val ssl: String,
	public val cr: String,
	public val loaderUrl: String,
	public val fflags: String,
	public val hl: String,
	public val cbr: String,
	public val fexp: String,
	public val title: String,
	public val enablejsapi: String,
	public val show_content_thumbnail: Boolean,
	public val enabled_engage_types: String,
	public val c: String,
	public val fmt_list: String,
	public actual val player_response: String,
	public val ucid: String,
	public val length_seconds: String,
	public actual val url_encoded_fmt_stream_map: String,
	public val gapi_hint_params: String,
	public actual val adaptive_fmts: String,
	public val cver: String,
	public val innertube_context_client_version: String,
	public val account_playback_token: String,
	public val timestamp: String,
	public val watermark: String,
	public val innertube_api_key: String,
	public val cos: String,
	public val vss_host: String,
	public val author: String
)

@Serializable
public actual class YTWatchPagePlayerConfig(public val attrs: YTVideoPlayerAttrs, public val sts: Double, public actual val args: YTWatchPagePlayerArgs, public actual val assets: YTVideoPlayerAssets) {
	public companion object {
		public val kSer: KSerializer<YTWatchPagePlayerConfig> = serializer()
	}
}
