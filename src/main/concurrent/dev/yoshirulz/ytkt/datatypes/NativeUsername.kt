package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.simple_http.querySelector
import dev.yoshirulz.simple_http.sendGETForHTML
import dev.yoshirulz.ytkt.RequiresHTMLParser
import io.ktor.client.*

@RequiresHTMLParser
public suspend fun Username.getChannelID(httpClient: HttpClient): ChannelID = ChannelID(
	this.canonicalURI.sendGETForHTML(httpClient)
		.querySelector("""meta[property="og:url"]""")!!
		.getAttribute("content")!!
		.substringAfter("channel/")
)

@Suppress("RedundantSuspendModifier", "UNUSED_PARAMETER")
public suspend fun Username.getData(httpClient: HttpClient): User = User(this)
