package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.sendGETForJSON
import dev.yoshirulz.ytkt.ytCanonicalURI
import io.ktor.client.*
import io.ktor.http.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce

public suspend fun SearchQuery.getPartialResults(httpClient: HttpClient, maxResults: Int): List<Video> = getResults(httpClient).let { resultGen ->
	try {
		val results = mutableListOf<Video>()
		while (results.size < maxResults) results.add(resultGen.receive())
		results
	} catch (_: ClosedReceiveChannelException) {
		emptyList()
	} finally {
		resultGen.cancel()
	}
}

public fun SearchQuery.getResults(httpClient: HttpClient): ReceiveChannel<Video> = GlobalScope.produce {
	var page = 1
	var tempPageResults: Array<YTVideoDetails>
	while (true) {
		tempPageResults = ytCanonicalURI(
			"/search_ajax",
			parametersOf(
				"style" to listOf("json"),
				"search_query" to listOf(query),
				"page" to listOf(page++.toString()),
				"hl" to listOf("en")
			)
		).sendGETForJSON(YTSearchResults.kSer, httpClient).video
		if (tempPageResults.isEmpty()) break
		tempPageResults.forEach { this.send(Video.fromJSON(it)) }
	}
}
