package dev.yoshirulz.ytkt

import dev.yoshirulz.simple_http.sendGETForPlaintext
import io.ktor.client.*
import kotlin.collections.set

public actual class YTKtCipherCache(private val httpClient: HttpClient) {
	private val cache = mutableMapOf<String, DecipherRoutine>()

	internal actual suspend fun decipher(signature: String, playerSourceURI: String)
		= cache.getOrPut(playerSourceURI) { getDecipherRoutine(sendGETForPlaintext(playerSourceURI, httpClient)) }
			.fold(signature) { acc, cipherOp -> cipherOp(acc) }
}
