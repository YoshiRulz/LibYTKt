package dev.yoshirulz.simple_http

import dev.yoshirulz.ytkt.RequiresHTMLParser
import dev.yoshirulz.ytkt.RequiresXMLParser
import dev.yoshirulz.ytkt.URI
import io.ktor.client.*
import io.ktor.client.request.*

@Suppress("NOTHING_TO_INLINE")
private inline fun noTODO(): Nothing = throw NotImplementedError()

@RequiresHTMLParser
@Suppress("NOTHING_TO_INLINE", "RedundantSuspendModifier", "unused", "UNUSED_PARAMETER")
private suspend inline fun HttpClient.getAndParseHTML(uri: String): HTMLPage = noTODO()

@RequiresHTMLParser
@Suppress("NOTHING_TO_INLINE", "RedundantSuspendModifier", "unused", "UNUSED_PARAMETER")
private suspend inline fun HttpClient.getAndParseHTML(uri: URI): HTMLPage = noTODO()

@RequiresXMLParser
@Suppress("NOTHING_TO_INLINE", "RedundantSuspendModifier", "unused", "UNUSED_PARAMETER")
private suspend inline fun HttpClient.getAndParseXML(uri: String): XMLPage = noTODO()

@RequiresXMLParser
@Suppress("NOTHING_TO_INLINE", "RedundantSuspendModifier", "unused", "UNUSED_PARAMETER")
private suspend inline fun HttpClient.getAndParseXML(uri: URI): XMLPage = noTODO()

public interface HTMLPageElement {
	public val textContent: String?
	public fun getAttribute(name: String): String?
}

public interface XMLPageElement {
	public fun attribute(name: String): String
	public fun descendants(s: String): List<XMLPageElement>
	public fun element(s: String): XMLPageElement?
}

public class HTMLPage

public class XMLPage

@Suppress("unused")
public val HTMLPage.pageSource: String get() = noTODO()

@Suppress("unused")
public val XMLPage.root: XMLPageElement get() = noTODO()

@RequiresHTMLParser
public suspend fun sendGETForHTML(uri: String, httpClient: HttpClient): HTMLPage = httpClient.getAndParseHTML(uri)

public suspend inline fun sendGETForPlaintext(uri: String, httpClient: HttpClient): String = httpClient.get(uri)

@RequiresXMLParser
public suspend fun sendGETForXML(uri: String, httpClient: HttpClient): XMLPage = httpClient.getAndParseXML(uri)

@Suppress("unused", "UNUSED_PARAMETER")
public fun HTMLPage.querySelector(s: String): HTMLPageElement? = noTODO()

@RequiresHTMLParser
public suspend fun URI.sendGETForHTML(httpClient: HttpClient): HTMLPage = httpClient.getAndParseHTML(this)

public suspend inline fun URI.sendGETForPlaintext(httpClient: HttpClient): String = httpClient.get(this.ktorUrl)

@RequiresXMLParser
public suspend fun URI.sendGETForXML(httpClient: HttpClient): XMLPage = httpClient.getAndParseXML(this)
