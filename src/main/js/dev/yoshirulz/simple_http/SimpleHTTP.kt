package dev.yoshirulz.simple_http

import kotlinx.browser.window
import org.w3c.dom.Document
import org.w3c.dom.XMLDocument
import org.w3c.dom.parsing.DOMParser
import kotlin.js.Promise

public fun sendGETForHTML(uri: String): Promise<Document> = window.fetch(uri)
	.then { response -> response.text().then { DOMParser().parseFromString(it, response.headers.get("Content-Type")!!.substringBefore(';')) } }
	.then { it }

public fun <T: Any> sendGETForJSON(uri: String): Promise<T> = window.fetch(uri).then { it.json() }.then { it.unsafeCast<T>() }

public fun sendGETForPlaintext(uri: String): Promise<String> = window.fetch(uri).then { it.text() }.then { it }

@Suppress("NOTHING_TO_INLINE")
public inline fun sendGETForXML(uri: String): Promise<XMLDocument> = sendGETForHTML(uri).then { it as XMLDocument }
