package dev.yoshirulz.ytkt

import kotlinx.coroutines.await
import kotlin.collections.set

public actual class YTKtCipherCache {
	private val cache = mutableMapOf<String, DecipherRoutine>()

	internal actual suspend fun decipher(signature: String, playerSourceURI: String)
		= cache.getOrPut(playerSourceURI) { getDecipherRoutine(proxiedGETForPlaintext(playerSourceURI).await()) }
			.fold(signature) { acc, cipherOp -> cipherOp(acc) }
}
