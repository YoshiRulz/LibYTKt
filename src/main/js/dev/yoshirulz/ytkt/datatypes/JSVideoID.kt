package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.*
import io.ktor.http.*
import kotlinx.coroutines.async
import kotlinx.coroutines.await
import kotlinx.coroutines.coroutineScope
import kotlin.js.Promise
import kotlin.time.seconds

private interface PlayerConfiguration {
	val playerSourceURI: String
	val validUntil: Timestamp
	val avMetadataJSON: Array<YTStreamFormat>? get() = null
	val adaptiveMetadataJSON: Array<YTAdaptiveStreamFormat>? get() = null
	val hlsManifestURI: String? get() = null
	val dashManifestURI: String? get() = null
	val avMetadataUrlEncoded: String? get() = null
	val adaptiveMetadataUrlEncoded: String? get() = null

	companion object {
		private class LivePlayerConfig(
			override val playerSourceURI: String,
			override val validUntil: Timestamp,
			override val avMetadataJSON: Array<YTStreamFormat>?,
			override val adaptiveMetadataJSON: Array<YTAdaptiveStreamFormat>?,
			override val hlsManifestURI: String
		): PlayerConfiguration

		private class VideoPlayerConfig(
			override val playerSourceURI: String,
			override val validUntil: Timestamp,
			override val avMetadataJSON: Array<YTStreamFormat>?,
			override val adaptiveMetadataJSON: Array<YTAdaptiveStreamFormat>?,
			override val dashManifestURI: String,
			override val avMetadataUrlEncoded: String,
			override val adaptiveMetadataUrlEncoded: String
		): PlayerConfiguration

		fun fromYTPlayerResponse(secondResponseJSON: YTPlayerResponse, playerSourceUrlRelative: String, validUntil: Timestamp, avMetadataUrlEncoded: String?, adaptiveMetadataUrlEncoded: String?)
			= "https://www.youtube.com$playerSourceUrlRelative".let { playerSourceUrl ->
				if (secondResponseJSON.videoDetails?.isLive == true)
					LivePlayerConfig(
						playerSourceUrl,
						validUntil,
						secondResponseJSON.streamingData!!.formats,
						secondResponseJSON.streamingData.adaptiveFormats,
						secondResponseJSON.streamingData.hlsManifestUrl!!
					)
				else
					VideoPlayerConfig(
						playerSourceUrl,
						validUntil,
						secondResponseJSON.streamingData!!.formats,
						secondResponseJSON.streamingData.adaptiveFormats,
						secondResponseJSON.streamingData.dashManifestUrl!!,
						avMetadataUrlEncoded!!,
						adaptiveMetadataUrlEncoded!!
					)
			}
	}
}

private suspend fun VideoID.getPlayerConfig(): PlayerConfiguration {
	// first, scrape the embed page
	val responseAJSON = JSON.parse<YTEmbedPagePlayerConfig>(
		URI(embedURI.ktorUrl.copy(parameters = parametersOf("disable_polymer", "true")))
			.proxiedGETForPlaintext()
			.await()
			.substringAfter("yt.setConfig({'PLAYER_CONFIG': ", EMPTY_STRING)
			.substringBefore("});writeEmbed();</script>", EMPTY_STRING)
			.ifEmpty { throw VideoUnplayableException(this) } // couldn't get the JSON string from the response
	)
	val requestTimestampB = Timestamp.now()
	val responseB = getVideoInfoMap().await()
	val responseBJSON = JSON.parse<YTPlayerResponse>(responseB["player_response"]!!)
	if ("error".equals(responseBJSON.playabilityStatus?.status, ignoreCase = true)) throw VideoUnavailableException(this)
	return if (responseBJSON.playabilityStatus?.reason.isNullOrBlank()) { // no error reason given means there was no error i.e. it succeeded, extract info and return
		PlayerConfiguration.fromYTPlayerResponse(
			responseBJSON,
			responseAJSON.assets.js,
			requestTimestampB + responseBJSON.streamingData!!.expiresInSeconds.seconds,
			responseB["url_encoded_fmt_stream_map"],
			responseB["adaptive_fmts"]
		)
	} else { // couldn't get the data linked from the embed page
		// two checks for premium (non-gratis) content
		responseBJSON.playabilityStatus?.errorScreen?.playerLegacyDesktopYpcTrailerRenderer?.trailerVideoId?.let {
			if (it.isNotBlank()) throw VideoRequiresPurchaseException(this, it)
		}
		responseBJSON.playabilityStatus?.errorScreen?.ypcTrailerRenderer?.playerVars?.let {
			if (it.isNotBlank()) throw VideoRequiresPurchaseException(this, it.parseUrlEncodedParameters()["video_id"]!!)
		}

		// there was some other error, move on to scraping the watch page
		val requestTimestampC = Timestamp.now()
		val responseCStructured = getVideoPageBody().await()
		val responseCRaw = responseCStructured.documentElement!!.outerHTML
			.substringAfter("ytplayer.config = ", EMPTY_STRING)
			.substringBefore(""";ytplayer.load = function() {yt.player.Application.create("player-api", ytplayer.config);ytplayer.config.loaded = true;};(function() {if (!!window.yt && yt.player && yt.player.Application) {ytplayer.load();}}());</script>""", EMPTY_STRING)
			.ifEmpty { throw VideoUnplayableException(this, responseCStructured.querySelector("#unavailable-message")?.textContent?.trim()) } // couldn't get the JSON string from the response
		val responseCJSON = JSON.parse<YTWatchPagePlayerConfig>(responseCRaw)
		val responseDJSON = JSON.parse<YTPlayerResponse>(responseCJSON.args.player_response)
		PlayerConfiguration.fromYTPlayerResponse(
			responseDJSON,
			responseCJSON.assets.js,
			requestTimestampC + responseDJSON.streamingData!!.expiresInSeconds.seconds,
			responseCJSON.args.url_encoded_fmt_stream_map,
			responseCJSON.args.adaptive_fmts
		)
	}
}

private fun VideoID.getVideoInfoMap() = ytCanonicalURI(
	"/get_video_info",
	parametersOf(
		"video_id" to listOf(raw),
		"el" to listOf("embedded"),
		"eurl" to listOf("https%3A%2F%2Fyoutube.googleapis.com%2Fv%2F$raw"),
		"hl" to listOf("en")
	)
).proxiedGETForPlaintext().then { it.parseUrlEncodedParameters() }

private fun VideoID.getVideoPageBody() = URI(canonicalURI.ktorUrl.copy(parameters = parametersOf(
	"v" to listOf(raw),
	"disable_polymer" to listOf("true"),
	"bpctr" to listOf("9999999999")
))).proxiedGETForHTML()

private fun VideoID.getVideoPlayerResponse() = getVideoInfoMap().then { JSON.parse<YTPlayerResponse>(it["player_response"]!!) }

public fun VideoID.getCCMetadata(): Promise<List<CCTrackMetadata>> = getVideoPlayerResponse().then { CCTrackMetadata.getListFromPlayerResponse(it, this) }

public suspend fun VideoID.getData(): Video {
	val playerResponseRequest = this.getVideoPlayerResponse()
	val watchPageBodyRequest = this.getVideoPageBody()
	val playerResponse = playerResponseRequest.await()
	if ("error".equals(playerResponse.playabilityStatus?.status, ignoreCase = true)) throw VideoUnavailableException(this)
	val videoDetails = playerResponse.videoDetails!!
	val watchPageBody = watchPageBodyRequest.await()
	return Video(
		this,
		videoDetails.author,
		Timestamp.fromYMDString(watchPageBody.querySelector("""meta[itemprop="datePublished"]""")!!.getAttribute("content")!!),
		videoDetails.title,
		videoDetails.shortDescription,
		videoDetails.lengthSeconds.seconds,
		videoDetails.keywords.orEmpty().asList(),
		videoDetails.viewCount?.toULong() ?: 0UL, // some videos have no viewCount
		watchPageBody.querySelector("button.like-button-renderer-like-button")?.textContent?.stripNonDigit()?.toULong() ?: 0UL,
		watchPageBody.querySelector("button.like-button-renderer-dislike-button")?.textContent?.stripNonDigit()?.toULong() ?: 0UL
	)
}

public suspend fun VideoID.getStreamMetadataSet(cipherCache: YTKtCipherCache): StreamMetadataSet = coroutineScope {
	val playerConfiguration = getPlayerConfig()

	val avStreamsFromMetadata = async {
		playerConfiguration.avMetadataUrlEncoded.orEmpty()
			.split(',')
			.filter { it.isNotEmpty() }
			.map { it.parseUrlEncodedParameters() }
			.mapNotNull { getAVStreamMetadata(it, playerConfiguration.playerSourceURI, cipherCache) }
			.toList()
	}
	val avStreamsFromJSON = playerConfiguration.avMetadataJSON?.let { list ->
		async { list.mapNotNull { getAVStreamMetadataFromJSON(it, playerConfiguration.playerSourceURI, cipherCache) } }
	}
	val adaptiveStreamsFromMetadata = async {
		getStreamMetadataFromAdaptiveList(cipherCache, playerConfiguration.playerSourceURI, playerConfiguration.adaptiveMetadataUrlEncoded.orEmpty())
	}
	val adaptiveStreamsFromJSON = playerConfiguration.adaptiveMetadataJSON?.let { list ->
		async { list.mapNotNull { getAdaptiveStreamMetadataFromJSON(it, playerConfiguration.playerSourceURI, cipherCache) } }
	}
	val adaptiveStreamsFromDASH = async {
		playerConfiguration.dashManifestURI?.takeIf { it.isNotBlank() }?.let {
			getStreamMetadataFromDASHManifest(cipherCache, playerConfiguration.playerSourceURI, it)
		} ?: Pair(emptyList(), emptyList())
	}

	val avStreamsMetadata = if (avStreamsFromJSON == null) avStreamsFromMetadata.await() else avStreamsFromMetadata.await() + avStreamsFromJSON.await()
	val audioStreamsMetadata = mutableListOf<AudioOnlyStreamMetadata>()
	val videoStreamsMetadata = mutableListOf<VideoOnlyStreamMetadata>()
	adaptiveStreamsFromMetadata.await().let { (a, v) ->
		audioStreamsMetadata += a
		videoStreamsMetadata += v
	}
	adaptiveStreamsFromJSON?.await()?.forEach {
		when (it) {
			is AudioOnlyStreamMetadata -> audioStreamsMetadata += it
			is VideoOnlyStreamMetadata -> videoStreamsMetadata += it
		}
	}
	adaptiveStreamsFromDASH.await().let { (a, v) ->
		audioStreamsMetadata += a
		videoStreamsMetadata += v
	}
	StreamMetadataSet(
		avStreamsMetadata.sortedByDescending { it.videoQuality },
		audioStreamsMetadata.sortedByDescending { it.bitrate },
		videoStreamsMetadata.sortedByDescending { it.videoQuality },
		playerConfiguration.validUntil,
		playerConfiguration.hlsManifestURI
	)
}
