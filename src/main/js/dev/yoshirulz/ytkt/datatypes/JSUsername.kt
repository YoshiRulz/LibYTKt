package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.EntryPoint
import dev.yoshirulz.ytkt.proxiedGETForHTML
import kotlin.js.Promise

public fun Username.getChannelID(): Promise<ChannelID> = this.canonicalURI.proxiedGETForHTML()
	.then { ChannelID(it.querySelector("""meta[property="og:url"]""")!!.getAttribute("content")!!.substringAfter("channel/")) }

@EntryPoint
public fun Username.getData(): User = User(this)
