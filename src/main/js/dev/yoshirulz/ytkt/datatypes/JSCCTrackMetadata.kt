package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.RelativeTimestamp
import dev.yoshirulz.ytkt.descendants
import dev.yoshirulz.ytkt.proxiedGETForXML
import kotlin.js.Promise

public fun CCTrackMetadata.getCaptions(): Promise<ClosedCaptionsTrack> = this.uri.proxiedGETForXML().then { xml ->
	xml.documentElement!!
		.descendants("p")
		.associateWith { it.toString() }
		.filterNot { (_, s) -> s.isEmpty() }
		.map { (e, s) ->
			val startTime = e.getAttribute("t")!!.toLong()
			CCEntry(s, RelativeTimestamp(startTime)..RelativeTimestamp(startTime + e.getAttribute("d")!!.toLong()))
		}
}
