@file:Suppress("SpellCheckingInspection")

package dev.yoshirulz.ytkt.datatypes

public actual class YTAdaptiveStreamFormat(
	public actual val itag: Double,
	public actual val url: String,
	public actual val mimeType: String,
	public actual val bitrate: Double,
	public actual val width: Double? = null,
	public actual val height: Double? = null,
	public actual val contentLength: String? = null,
	public actual val fps: Double? = null,
	public actual val qualityLabel: String? = null,
	public actual val audioSampleRate: String? = null,
	public actual val cipher: String
)

public actual class YTCCTrack(
	public actual val baseUrl: String,
	public actual val name: YTRichText,
	public actual val vssId: String,
	public actual val languageCode: String
)

public actual class YTCCTrackDetails(public actual val captionTracks: Array<YTCCTrack>? = null)

public actual class YTCCTrackDetailsContainer(public actual val playerCaptionsTracklistRenderer: YTCCTrackDetails)

public actual class YTEmbedPagePlayerConfig(public actual val assets: YTVideoPlayerAssets)

public actual class YTPlayerResponse(
	public actual val streamingData: YTPlayerResponseStreamingData? = null,
	public actual val playabilityStatus: YTPlayerResponsePlayabilityStatus? = null,
	public actual val captions: YTCCTrackDetailsContainer? = null,
	public actual val videoDetails: YTPlayerResponseVideoDetails? = null
)

public actual class YTPlayerResponseErrorScreenDetails(
	public actual val playerLegacyDesktopYpcTrailerRenderer: YTPlayerResponseLegacyPremiumTrailerDetails? = null,
	public actual val ypcTrailerRenderer: YTPlayerResponsePremiumTrailerDetails? = null
)

public actual class YTPlayerResponseLegacyPremiumTrailerDetails(public actual val trailerVideoId: String)

public actual class YTPlayerResponsePlayabilityStatus(
	public actual val status: String? = null,
	public actual val reason: String? = null,
	public actual val errorScreen: YTPlayerResponseErrorScreenDetails? = null
)

public actual class YTPlayerResponsePremiumTrailerDetails(public actual val playerVars: String)

public actual class YTPlayerResponseStreamingData(
	public actual val expiresInSeconds: Double,
	public actual val formats: Array<YTStreamFormat>? = null,
	public actual val adaptiveFormats: Array<YTAdaptiveStreamFormat>? = null,
	public actual val dashManifestUrl: String? = null,
	public actual val hlsManifestUrl: String? = null
)

public actual class YTPlayerResponseVideoDetails(
	public actual val author: String,
	public actual val viewCount: Double? = null,
	public actual val title: String,
	public actual val shortDescription: String,
	public actual val isLive: Boolean? = null,
	public actual val lengthSeconds: Double,
	public actual val keywords: Array<String>? = null
)

public actual class YTPlaylistDetails(
	public actual val title: String,
	public actual val description: String? = null,
	public actual val views: Double? = null,
	public actual val video: Array<YTVideoDetails>? = null,
	public actual val author: String? = null
)

public actual class YTRichText(public actual val simpleText: String)

public actual class YTSearchResults(public actual val video: Array<YTVideoDetails>)

public actual class YTStreamFormat(
	public actual val itag: Double,
	public actual val url: String,
	public actual val mimeType: String,
	public actual val contentLength: String? = null,
	public actual val cipher: String
)

public actual class YTVideoDetails(
	public actual val time_created: Double,
	public actual val description: String,
	public actual val dislikes: Double,
	public actual val title: String,
	public actual val author: String,
	public actual val keywords: String,
	public actual val length_seconds: Double,
	public actual val views: String,
	public actual val encrypted_id: String,
	public actual val likes: Double
)

public actual class YTVideoPlayerAssets(public actual val js: String)

public actual class YTWatchPagePlayerArgs(
	public actual val player_response: String,
	public actual val url_encoded_fmt_stream_map: String,
	public actual val adaptive_fmts: String
)

public actual class YTWatchPagePlayerConfig(
	public actual val args: YTWatchPagePlayerArgs,
	public actual val assets: YTVideoPlayerAssets
)
