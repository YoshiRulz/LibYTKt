package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.*
import io.ktor.http.*
import kotlinx.coroutines.await

private val REGEX_A = Regex("""clen[/=](\d+)""")

private val REGEX_C = Regex("""/s/([^/]*)""") //TODO doesn't need to be regex

@Suppress("DuplicatedCode")
internal suspend fun getAdaptiveStreamMetadataFromJSON(streamInfoJson: YTAdaptiveStreamFormat, playerSourceURI: String, cipherCache: YTKtCipherCache): StreamMetadata? {
	val url = streamInfoJson.url.takeIf { it.isNotBlank() || playerSourceURI.isBlank() }
		?.parseURI()
		?: streamInfoJson.cipher.parseUrlEncodedParameters().let { qParams ->
			qParams["url"]!!.parseURI().copyAndSetParam(
				qParams["sp"] ?: "signature",
				cipherCache.decipher(qParams["s"]!!, playerSourceURI)
			)
		}
	var contentLength = streamInfoJson.contentLength?.toULong() ?: REGEX_A.find(url.toString())!!.groupValues[1].toULongOrNull() ?: 0UL
	if (contentLength == 0UL) { // couldn't extract content length, get it manually
		contentLength = getContentLength(url) ?: 0UL
		if (contentLength == 0UL) return null // still not available, stream is gone or faulty
	}
	val iTag = ITag(streamInfoJson.itag.toInt())
	val container = Container.parse(streamInfoJson.mimeType.substringBefore(';').substringAfter('/'))!!
	val bitrate = streamInfoJson.bitrate.toULong()
	return if (streamInfoJson.audioSampleRate != null)
			AudioOnlyStreamMetadata(
				iTag,
				url,
				container,
				contentLength,
				bitrate,
				AudioEncoding.parse(streamInfoJson.mimeType.substringAfter("codecs=\"").substringBefore('"'))!!
			)
		else
			VideoOnlyStreamMetadata(
				iTag,
				url,
				container,
				contentLength,
				bitrate,
				VideoEncoding.parse(streamInfoJson.mimeType.substringAfter("codecs=\"").substringBefore('"'))!!,
				VideoQuality.parse(streamInfoJson.qualityLabel!!),
				VideoResolution(streamInfoJson.width!!.toULong().toUShort(), streamInfoJson.height!!.toULong().toUShort()),
				streamInfoJson.fps!!.toUInt()
			)
}

internal suspend fun getAVStreamMetadata(qParams: Parameters, playerSourceURI: String, cipherCache: YTKtCipherCache): AudioVisualStreamMetadata? {
	val uri = qParams["url"]!!.parseURI().let {
		val signature = qParams["s"] // if set, decipher and add to the new URI
		if (signature.isNullOrBlank()) it
		else it.copyAndSetParam(
			qParams["sp"] ?: "signature",
			cipherCache.decipher(signature, playerSourceURI)
		)
	}
	var contentLength = REGEX_A.find(uri.toString())!!.groupValues[1].toULongOrNull() ?: 0UL
	if (contentLength == 0UL) { // couldn't extract content length, get it manually
		contentLength = getContentLength(uri) ?: 0UL
		if (contentLength == 0UL) return null // still not available, stream is gone or faulty
	}
	val type = qParams["type"]!!
	val avCodecs = type.substringAfter('"').substringBefore('"').split(' ', limit = 2).map { it.substringBefore('.') }
	val iTag = ITag(qParams["itag"]!!.toInt())
	val videoQuality = iTag.videoQuality!!
	return AudioVisualStreamMetadata(
		iTag,
		uri,
		Container.parse(type.substringAfter('/').substringBefore(';'))!!,
		contentLength,
		GET_BITRATE_FROM_TEST_DATA,
		AudioEncoding.parse(avCodecs[1])!!,
		VideoEncoding.parse(avCodecs[0])!!,
		videoQuality,
		videoQuality.canonicalRes,
		GET_FRAMERATE_FROM_TEST_DATA
	)
}

@Suppress("DuplicatedCode")
internal suspend fun getAVStreamMetadataFromJSON(streamInfoJson: YTStreamFormat, playerSourceURI: String, cipherCache: YTKtCipherCache): AudioVisualStreamMetadata? {
	val url = streamInfoJson.url.takeIf { it.isNotBlank() || playerSourceURI.isBlank() }
		?.parseURI()
		?: streamInfoJson.cipher.parseUrlEncodedParameters().let { qParams ->
			qParams["url"]!!.parseURI().copyAndSetParam(
				qParams["sp"] ?: "signature",
				cipherCache.decipher(qParams["s"]!!, playerSourceURI)
			)
		}
	var contentLength = streamInfoJson.contentLength?.toULong() ?: REGEX_A.find(url.toString())!!.groupValues[1].toULongOrNull() ?: 0UL
	if (contentLength == 0UL) { // couldn't extract content length, get it manually
		contentLength = getContentLength(url) ?: 0UL
		if (contentLength == 0UL) return null // still not available, stream is gone or faulty
	}
	val avCodecs = streamInfoJson.mimeType.substringAfter("codecs=\"").substringBefore("\"").split(", ")
	val iTag = ITag(streamInfoJson.itag.toInt())
	val videoQuality = iTag.videoQuality!!
	return AudioVisualStreamMetadata(
		iTag,
		url,
		Container.parse(streamInfoJson.mimeType.substringBefore(";").substringAfter("/"))!!,
		contentLength,
		GET_BITRATE_FROM_TEST_DATA,
		AudioEncoding.parse(avCodecs[1])!!,
		VideoEncoding.parse(avCodecs[0])!!,
		videoQuality,
		videoQuality.canonicalRes,
		GET_FRAMERATE_FROM_TEST_DATA
	)
}

internal suspend fun getStreamMetadataFromAdaptiveList(cipherCache: YTKtCipherCache, playerSourceURI: String, adaptiveMetadataUrlEncoded: String): AdaptiveStreamMetadataSet {
	val audioStreamsMetadata = mutableListOf<AudioOnlyStreamMetadata>()
	val videoStreamsMetadata = mutableListOf<VideoOnlyStreamMetadata>()
	for (qParams in adaptiveMetadataUrlEncoded
		.split(',')
		.filter { it.isNotEmpty() }
		.map{ it.parseUrlEncodedParameters() }
	) {
		val iTag = ITag(qParams["itag"]!!.toInt())
		val bitrate = qParams["bitrate"]!!.toULong()
		val uri = qParams["url"]!!.parseURI().let {
			val signature = qParams["s"] // if set, decipher and add to the new URI
			if (signature.isNullOrBlank()) it
			else it.copyAndSetParam(
				qParams["sp"] ?: "signature",
				cipherCache.decipher(signature, playerSourceURI)
			)
		}
		val container = Container.parse(qParams["type"]!!.substringAfter('/').substringBefore(';'))!!
		var contentLength = REGEX_A.find(uri.toString())!!.groupValues[1].toULongOrNull() ?: 0UL
		if (contentLength == 0UL) { // couldn't extract content length, get it manually
			contentLength = getContentLength(uri) ?: 0UL
			if (contentLength == 0UL) continue // still not available, stream is gone or faulty
		}
		if (qParams["type"]!!.startsWith("audio/")) { // audio-only
			audioStreamsMetadata.add(AudioOnlyStreamMetadata(
				iTag,
				uri,
				container,
				contentLength,
				bitrate,
				AudioEncoding.parse(qParams["type"]!!.substringAfter('"').substringBefore('.'))!!
			))
		} else { // video-only
			videoStreamsMetadata.add(VideoOnlyStreamMetadata(
				iTag,
				uri,
				container,
				contentLength,
				bitrate,
				VideoEncoding.parse(qParams["type"]!!.substringAfter('"').substringBefore('.'))!!,
				VideoQuality.parse(qParams["quality_label"]!!.substringBefore('p') + 'p'),
				VideoResolution.parse(qParams["size"]!!),
				qParams["fps"]!!.toUInt()
			))
		}
	}
	return AdaptiveStreamMetadataSet(audioStreamsMetadata, videoStreamsMetadata)
}

internal suspend fun getStreamMetadataFromDASHManifest(cipherCache: YTKtCipherCache, playerSourceURI: String, dashManifestUrlOrig: String): AdaptiveStreamMetadataSet {
	val audioStreamsMetadata = mutableListOf<AudioOnlyStreamMetadata>()
	val videoStreamsMetadata = mutableListOf<VideoOnlyStreamMetadata>()
	val dashManifestUrl = REGEX_C.find(dashManifestUrlOrig)?.let { match ->
		match.groupValues[1].takeIf { it.isNotBlank() }
	}?.let { // need to decipher signature and set it in the URI
		dashManifestUrlOrig.setURIPathEncParam("signature", cipherCache.decipher(it, playerSourceURI))
	} ?: dashManifestUrlOrig
	for (streamInfoXML in proxiedGETForXML(dashManifestUrl)
		.await()
		.documentElement!!
		.descendants("Representation") // get representation nodes from DASH manifest
		.filter { it.descendants("Initialization").firstOrNull()?.getAttribute("sourceURL")?.contains("sq/") != true } // skip partial streams
	) {
		val iTag = ITag(streamInfoXML.getAttribute("id")!!.toInt())
		val uri = streamInfoXML.element("BaseURL")!!.toString()
		val (rawContainer, rawContentLength)
			= if (uri.contains('?')) uri.parseUrlEncodedParameters().let { params -> Pair(params["mime"]!!.substringAfter('/'), params["clen"]!!) }
				else Pair(uri.substringAfter("/mime/").substringBefore('/').decodeURLQueryComponent(), uri.substringAfter("/clen/").substringBefore('/').decodeURLQueryComponent())
		val (container, contentLength) = Pair(Container.parse(rawContainer)!!, rawContentLength.toULong())
		val bitrate = streamInfoXML.getAttribute("bandwidth")!!.toULong()
		if (streamInfoXML.element("AudioChannelConfiguration") != null) { // audio-only
			audioStreamsMetadata.add(AudioOnlyStreamMetadata(
				iTag,
				uri.parseURI(),
				container,
				contentLength,
				bitrate,
				AudioEncoding.parse(streamInfoXML.getAttribute("codecs")!!)!!
			))
		} else { // video-only
			videoStreamsMetadata.add(VideoOnlyStreamMetadata(
				iTag,
				uri.parseURI(),
				container,
				contentLength,
				bitrate,
				VideoEncoding.parse(streamInfoXML.getAttribute("codecs")!!)!!,
				iTag.videoQuality!!,
				VideoResolution(streamInfoXML.getAttribute("width")!!.toUShort(), streamInfoXML.getAttribute("height")!!.toUShort()),
				streamInfoXML.getAttribute("frameRate")!!.toUInt()
			))
		}
	}
	return AdaptiveStreamMetadataSet(audioStreamsMetadata, videoStreamsMetadata)
}
