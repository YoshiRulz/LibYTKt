package dev.yoshirulz.ytkt.datatypes

import dev.yoshirulz.ytkt.proxiedGETForHTML
import kotlin.js.Promise

public fun ChannelID.getData(): Promise<Channel> = canonicalURI.proxiedGETForHTML().then {
	Channel(
		this,
		it.querySelector("""meta[property="og:title"]""")!!.getAttribute("content")!!,
		it.querySelector("""meta[property="og:image"]""")!!.getAttribute("content")!!
	)
}
