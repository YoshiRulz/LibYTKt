package dev.yoshirulz.ytkt

import org.w3c.dom.Element
import kotlin.js.Date
import kotlin.math.roundToLong

private inline fun Date.toTimestamp() = Timestamp(getTime().roundToLong())

internal fun getContentLength(@Suppress("UNUSED_PARAMETER") requestUri: URI) = 0UL as ULong? //TODO return content length from GET (HEADER?) response headers, null if response status code isn't 2xx

@Suppress("UNCHECKED_CAST")
internal inline fun Element.descendants(s: String) = (this.childNodes as Iterable<Element>).filter { it.tagName.equals(s, ignoreCase = true) }

@Suppress("UNCHECKED_CAST")
internal inline fun Element.element(s: String) = (this.childNodes as Iterable<Element>).firstOrNull { it.tagName.equals(s, ignoreCase = true) }

public actual fun platformTimeFromYMD(year: Int, month: Int, day: Int): Timestamp = Date(year, month - 1, day).toTimestamp()

public actual fun platformTimeNow(): Timestamp = Date(Date.now()).toTimestamp()
