package dev.yoshirulz.ytkt

import dev.yoshirulz.ytkt.datatypes.StreamMetadataSet
import dev.yoshirulz.ytkt.datatypes.VideoID
import dev.yoshirulz.ytkt.datatypes.getStreamMetadataSet

public actual class YTKtScraperCache {
	public val cipherCache: YTKtCipherCache = YTKtCipherCache()

	public actual suspend inline fun VideoID.getStreamMetadataSet(): StreamMetadataSet = this.getStreamMetadataSet(this@YTKtScraperCache.cipherCache)

	public actual companion object {
		public actual fun createDefaultInstance(): YTKtScraperCache = YTKtScraperCache()
	}
}
