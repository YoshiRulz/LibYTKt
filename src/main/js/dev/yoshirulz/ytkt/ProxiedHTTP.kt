package dev.yoshirulz.ytkt

import dev.yoshirulz.simple_http.sendGETForHTML
import dev.yoshirulz.simple_http.sendGETForJSON
import dev.yoshirulz.simple_http.sendGETForPlaintext
import dev.yoshirulz.simple_http.sendGETForXML

@Suppress("NOTHING_TO_INLINE")
private inline fun String.addProxyPrefix() = "https://cors-anywhere.herokuapp.com/$this"

@Suppress("NOTHING_TO_INLINE")
internal inline fun proxiedGETForHTML(uri: String) = sendGETForHTML(uri.addProxyPrefix())

@Suppress("NOTHING_TO_INLINE")
internal inline fun <T: Any> proxiedGETForJSON(uri: String) = sendGETForJSON<T>(uri.addProxyPrefix())

@Suppress("NOTHING_TO_INLINE")
internal inline fun proxiedGETForPlaintext(uri: String) = sendGETForPlaintext(uri.addProxyPrefix())

@Suppress("NOTHING_TO_INLINE")
internal inline fun proxiedGETForXML(uri: String) = sendGETForXML(uri.addProxyPrefix())

@Suppress("NOTHING_TO_INLINE")
internal inline fun URI.proxiedGETForHTML() = sendGETForHTML(this.toString().addProxyPrefix())

@Suppress("NOTHING_TO_INLINE")
internal inline fun <T: Any> URI.proxiedGETForJSON() = sendGETForJSON<T>(this.toString().addProxyPrefix())

@Suppress("NOTHING_TO_INLINE")
internal inline fun URI.proxiedGETForPlaintext() = sendGETForPlaintext(this.toString().addProxyPrefix())

@Suppress("NOTHING_TO_INLINE")
internal inline fun URI.proxiedGETForXML() = sendGETForXML(this.toString().addProxyPrefix())
