@file:Suppress("SpellCheckingInspection")

import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

plugins {
	kotlin("multiplatform") version "1.4.0"
	id("com.github.ben-manes.versions") version "0.29.0"
}

repositories {
	jcenter()
}

kotlin {
	js(IR) {
		binaries.executable() // :browserProductionWebpack
		browser {}
	}

	@Suppress("UNUSED_VARIABLE")
	sourceSets {
		fun creatingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {}) = creating {
			parent?.let(::dependsOn)
			configuration(this)
		}

		fun gettingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {}) = getting {
			parent?.let(::dependsOn)
			configuration(this)
		}

		val commonMain by gettingWithParent(null)

		val jsMain by gettingWithParent(commonMain)

		all {
			kotlin.srcDir(name.takeLast(4).let { "src/${it.toLowerCase()}/${name.removeSuffix(it)}" })
			languageSettings.enableLanguageFeature("InlineClasses")
//			languageSettings.useExperimentalAnnotation("kotlin.RequiresOptIn")
		}
	}
}
