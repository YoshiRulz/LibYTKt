@file:Suppress("SpellCheckingInspection")

import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

plugins {
	kotlin("multiplatform") version "1.4.20"
	kotlin("plugin.serialization") version "1.4.20"
	`maven-publish`
	id("com.github.ben-manes.versions") version "0.36.0"
}

group = "dev.yoshirulz"
version = "0.3.0"

repositories {
	jcenter()
	maven("https://kotlin.bintray.com/ktor")
}

kotlin {
//	androidNativeArm64()
	js(IR) {
		browser {
			testTask {
				useKarma {
					useFirefox()
				}
			}
		}
	}
	jvm()
	linuxX64()
	mingwX64()

	explicitApi()

	@Suppress("UNUSED_VARIABLE")
	sourceSets {
		fun creatingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {}) = creating {
			parent?.let(::dependsOn)
			configuration(this)
		}

		fun gettingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {}) = getting {
			parent?.let(::dependsOn)
			configuration(this)
		}

		fun depStr(partialCoords: String, version: String) = "$partialCoords:$version"

		fun kotlinx(module: String, version: String) = depStr("org.jetbrains.kotlinx:kotlinx-$module", version)

		fun coroutines(module: String) = kotlinx("coroutines-$module", "1.4.1")

		fun ktorClient(module: String) = depStr("io.ktor:ktor-client-$module", "1.4.1")

		val commonMain by gettingWithParent(null) {
			dependencies {
				implementation(kotlinx("serialization-core", "1.0.1"))
				implementation(coroutines("core"))
				implementation(ktorClient("core"))
				implementation(ktorClient("json"))
				implementation(ktorClient("serialization"))
				depStr("net.devrieze:xmlutil", "0.80.1")
				depStr("net.devrieze:xmlutil-serialization", "0.80.1")
			}
		}
		val commonTest by gettingWithParent(null) {
			dependencies {
				implementation(kotlin("test-annotations-common"))
				implementation(kotlin("test-common"))
				implementation(ktorClient("mock"))
			}
		}

		val jsMain by gettingWithParent(commonMain) {
			dependencies {
				implementation(coroutines("core-js"))
				implementation(ktorClient("js"))
			}
		}
		val jsTest by gettingWithParent(commonTest) {
			dependencies {
				implementation(kotlin("test-js"))
			}
		}

		val concurrentMain by creatingWithParent(commonMain)
		val concurrentTest by creatingWithParent(commonTest)

		val jvmMain by gettingWithParent(concurrentMain) {
			dependencies {
				implementation(ktorClient("cio"))
			}
		}
		val jvmTest by gettingWithParent(concurrentTest) {
			dependencies {
				implementation(kotlin("test"))
			}
		}

		val nativeMain by creatingWithParent(concurrentMain) {
			dependencies {
				implementation(ktorClient("curl"))
			}
		}
		val nativeTest by creatingWithParent(concurrentTest)

//		val androidNativeArm64Main by gettingWithParent(nativeMain)
//		val androidNativeArm64Test by gettingWithParent(nativeTest)

		val linuxX64Main by gettingWithParent(nativeMain)
		val linuxX64Test by gettingWithParent(nativeTest)

		val mingwX64Main by gettingWithParent(nativeMain)
		val mingwX64Test by gettingWithParent(nativeTest)

		all {
			kotlin.srcDir(name.takeLast(4).let { "src/${it.toLowerCase()}/${name.removeSuffix(it)}" })
			languageSettings.enableLanguageFeature("InlineClasses")
		}
	}
}
