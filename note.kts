fun runBlocking(block: suspend CoroutineScope.() -> Unit) {
	GlobalScope.launch(Dispatchers.Default, block = block)
}

fun main() = println("read this file")
